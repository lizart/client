export const events = {
  namespaced: true,
  state: {
    events: []
  },
  mutations: {
    setEvents(state, events) {
      state.events = events;
    }
  },
  actions: {
    async getEvents({ commit }) {
      const events:Object = await this.$axios.$get(
        "./events-short.json"
      );
      commit("setEvents", events);
    }
  }
}
