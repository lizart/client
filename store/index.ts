import {profile} from './user/profile'
import {events} from './events/events'
export const state = () => ({
  people: []
})

export const mutations = {
  setPeople(state, people) {
    state.people = people
  }
}

export const actions = {
  async nuxtServerInit({ commit }, { app }) {
    const people:Object[] = await app.$axios.$get(
      "./data.json"
    )
    commit("setPeople", people)
  }
}
export const modules = {
  profile: profile,
  events: events
}
