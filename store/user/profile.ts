export const profile = {
  namespaced: true,
  state: {
    profile: {}
  },
  mutations: {
    setProfile(state, profile) {
      state.profile = profile
    }
  },
  actions: {
    async getProfile({ commit }) {
      const profile:Object = await this.$axios.$get(
        "./profile.json"
      )
      commit("setProfile", profile)
    }
  }
}
