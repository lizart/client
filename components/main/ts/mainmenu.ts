import {
  Component,
  Prop,
  Vue
} from "nuxt-property-decorator"

import { State, Getter, Action, namespace } from "vuex-class"

const profile = namespace('profile')

@Component({
  created () {
    this.getProfile();
  },
  watch: {
      stateProfile () {
        this.$refs.ava.style.backgroundImage = `url(${this.stateProfile.avatar})`;
      }
  },
  methods: {
    initProfile () {
      console.log("initProfile")
    },
    initEventCreation () {
      console.log('initEventCreation')
    }
  }
})

export default class MainMenu extends Vue {
  @profile.Action('getProfile') getProfile
  @profile.State('profile') stateProfile
}
